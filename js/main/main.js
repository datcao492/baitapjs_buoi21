let danhSachNhanVien = [];

const timKimViTri = function (id, array) {
  return array.findIndex(function (nv) {
    return nv.taiKhoan == id;
  });
};

//Lưu LOCAL-STORAGE
const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";
const luuLocalStorage = () => {
  let dsnvJson = JSON.stringify(danhSachNhanVien);
  localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
};
//lấy dữ liệu từ localstorage khi user tải lại trang
let dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);
// Gán cho array gôc và render lại giao diện
if (dsnvJson) {
  danhSachNhanVien = JSON.parse(dsnvJson);
  danhSachNhanVien = danhSachNhanVien.map((item) => {
    return new NhanVien(
      item.taiKhoan,
      item.hoTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luong,
      item.chucVu,
      item.gioLam
    );
  });
  renderDanhSachNhanVien(danhSachNhanVien);
}

//reset lại modal thêm nhân viên
document.getElementById("btnThem").addEventListener("click", () => {
  document.getElementById("form_NV").reset();
  document.getElementById("btnCapNhat").style.display = "none";
  document.getElementById("btnThemNV").style.display = "block";
});


function themNhanVien() {
  let newNhanVien = layThongTinTuForm();

  let isValid = isValidAll();

  if (isValid) {
    danhSachNhanVien.push(newNhanVien);
    renderDanhSachNhanVien(danhSachNhanVien);
    luuLocalStorage();
    $("#myModal").modal("hide");
  }
}

function xoaNhanVien(id) {
  let viTri = timKimViTri(id, danhSachNhanVien);

  danhSachNhanVien.splice(viTri, 1);
  renderDanhSachNhanVien(danhSachNhanVien);
  luuLocalStorage();
}


function suaNhanVien(id) {
  document.getElementById("btnCapNhat").style.display = "block";
  document.getElementById("btnThemNV").style.display = "none";
  let viTri = timKimViTri(id, danhSachNhanVien);
  let nhanVien = danhSachNhanVien[viTri];

  xuatThongTinLenForm(nhanVien);
}


function capNhatNhanVien() {
  let nhanVienEdit = layThongTinTuForm();
  let viTri = timKimViTri(nhanVienEdit.taiKhoan, danhSachNhanVien);
  danhSachNhanVien[viTri] = nhanVienEdit;

  let isValid = isValidAllCapNhat();
  if (isValid) {
    renderDanhSachNhanVien(danhSachNhanVien);
    luuLocalStorage();
    $("#myModal").modal("hide");
  }
}


function showDSNV() {
  let searchName = document.getElementById("searchName").value;
  if (searchName == "") {
    renderDanhSachNhanVien(danhSachNhanVien);
  }
}


document.getElementById("btnTimNV").addEventListener("click", function () {
  let staffTypeList = [];
  let searchType = document.getElementById("searchStaff").value;
  if (searchType == "1") {
    staffTypeList = danhSachNhanVien.filter(function (item) {
      return item.gioLam >= 192;
    });
    renderDanhSachNhanVien(staffTypeList);
  } else if (searchType == "2") {
    staffTypeList = danhSachNhanVien.filter(function (item) {
      return item.gioLam >= 176 && item.gioLam < 192;
    });
    renderDanhSachNhanVien(staffTypeList);
  } else if (searchType == "3") {
    staffTypeList = danhSachNhanVien.filter(function (item) {
      return item.gioLam >= 160 && item.workingHours < 176;
    });
    renderDanhSachNhanVien(staffTypeList);
  } else if (searchType == "4") {
    staffTypeList = danhSachNhanVien.filter(function (item) {
      return item.gioLam < 160;
    });
    renderDanhSachNhanVien(staffTypeList);
  } else {
    renderDanhSachNhanVien(danhSachNhanVien);
  }
});