function layThongTinTuForm() {
  let taiKhoan = document.getElementById("tknv").value;
  let hoTen = document.getElementById("name").value;
  let email = document.getElementById("email").value;
  let matKhau = document.getElementById("password").value;
  let ngayLam = document.getElementById("datepicker").value;
  let luong = document.getElementById("luongCB").value * 1;
  let chucVu = document.getElementById("chucvu").value;
  let gioLam = document.getElementById("gioLam").value * 1;

  let nhanVien = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luong,
    chucVu,
    gioLam
  );
  return nhanVien;
}

function renderDanhSachNhanVien(dsnv) {
  let contentHTML = "";
  for (let i = 0; i < dsnv.length; i++) {
    let nhanVien = dsnv[i];
    let contentTr = `<tr>
        <td>${nhanVien.taiKhoan}</td>
        <td>${nhanVien.hoTen}</td>
        <td>${nhanVien.email}</td>
        <td>${nhanVien.ngayLam}</td>
        <td>${nhanVien.chucVu}</td>
        <td>${nhanVien.luong}</td>
        <td>${nhanVien.gioLam}</td>
        <td>${nhanVien.tinhLuong()}</td>
        <td>${nhanVien.xepLoai()}</td>

        <td>
            <button data-toggle="modal" data-target="#myModal" onclick="suaNhanVien('${nhanVien.taiKhoan}')" class="btn btn-success">EDIT</button>

            <button onclick="xoaNhanVien('${nhanVien.taiKhoan}')" class="btn btn-danger">DELETE</button>
        </td>
    </tr>`;

    contentHTML += contentTr;
  }

  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function xuatThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.hoTen;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luong;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
