function ValidatorNV() {
  this.kiemTraRong = function (idTarget, idErr, messageErr) {
    let input = document.getElementById(idTarget).value.trim();

    if (input == "") {
      document.getElementById(idErr).innerText = messageErr;
      return false;
    } else {
      document.getElementById(idErr).innerText = "";
      return true;
    }
  };

  this.kiemTraHoTen = function () {
    let input = document.getElementById("name").value;
    let letters =
      /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    if (letters.test(input)) {
      document.getElementById("tbTen").innerText = "";
      return true;
    } else {
      document.getElementById("tbTen").innerText = "Tên không hợp lệ";
      return false;
    }
  };

  this.kiemTraEmailHopLe = function (idTarget, idError) {
    let parten = /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/;

    let input = document.getElementById(idTarget).value;

    if (parten.test(input)) {
      document.getElementById(idError).innerText = "";
      return true;
    }

    document.getElementById(idError).innerText = "Email không hợp lệ";
  };

  this.kiemTraMatKhau = function (idTarget, idError) {
    let parten =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,8}$/;

    let input = document.getElementById(idTarget).value;

    if (parten.test(input)) {
      document.getElementById(idError).innerText = "";
      return true;
    }

    document.getElementById(idError).innerText = "Mật Khẩu Không Hợp lệ";
  };

  this.kiemTraLuong = (idTarget, idError) => {
    let input = document.getElementById(idTarget).value;

    if (input >= 1e6 && input <= 20e6) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = "Lương từ 1tr đến 20tr";
    }
  };

  this.kiemTraGioLam = (idTarget, idError) => {
    let input = document.getElementById(idTarget).value;

    if (input >= 80 && input <= 200) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText =
        "Giờ làm từ 80h - 200h/tháng";
    }
  };

  this.kiemTraChucVu = function (value, idError, messageError) {
    let valueTarget = document.getElementById(value);

    if (valueTarget.selectedIndex == "") {
      document.getElementById(idError).innerText = messageError;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };

  this.kiemTraTrung = function (newNhanVien, danhSachNhanVien) {
    let i = danhSachNhanVien.findIndex((item) => {
      return item.taiKhoan == newNhanVien.taiKhoan;
    });

    if (i == -1) {
      document.getElementById("tbTKNV").innerText = "";
      return true;
    } else {
      document.getElementById("tbTKNV").innerText = "Tài Khoản trùng";
      return false;
    }
  };
}

let validatorNV = new ValidatorNV();
function isValidAll() {
  let newNhanVien = layThongTinTuForm();

  isValidTK =
    validatorNV.kiemTraRong("tknv", "tbTKNV", "Không được rỗng") &&
    validatorNV.kiemTraTrung(newNhanVien, danhSachNhanVien);

  isValidHoTen = validatorNV.kiemTraRong(
    "name",
    "tbTen",
    "Họ Tên không được rỗng"
  );

  isValidEmail =
    validatorNV.kiemTraRong("email", "tbEmail", "Email không được để rỗng") &&
    validatorNV.kiemTraEmailHopLe("email", "tbEmail");

  isValidMatKhau =
    validatorNV.kiemTraRong(
      "password",
      "tbMatKhau",
      "Vui lòng điền mật khẩu"
    ) && validatorNV.kiemTraMatKhau("password", "tbMatKhau");

  isValidNgayLam = validatorNV.kiemTraRong(
    "datepicker",
    "tbNgay",
    "Vui lòng điền ngày làm"
  );

  isValidLuong =
    validatorNV.kiemTraRong(
      "luongCB",
      "tbLuongCB",
      "Vui lòng nhập"
    ) && validatorNV.kiemTraLuong("luongCB", "tbLuongCB");

  isValidChucVu = validatorNV.kiemTraChucVu(
    "chucvu",
    "tbChucVu",
    "Chọn chức vụ"
  );

  isValidGioLam =
    validatorNV.kiemTraRong("gioLam", "tbGiolam", "Nhập giờ làm") &&
    validatorNV.kiemTraGioLam("gioLam", "tbGiolam");

  return (
    isValidTK &&
    isValidHoTen &&
    isValidEmail &&
    isValidMatKhau &&
    isValidNgayLam &&
    isValidLuong &&
    isValidChucVu &&
    isValidGioLam
  );
}

function isValidAllCapNhat() {
  isValidTK = validatorNV.kiemTraRong(
    "tknv",
    "tbTKNV",
    "Tài khoản không được rỗng"
  );

  isValidHoTen = validatorNV.kiemTraRong(
    "name",
    "tbTen",
    "Vui lòng không để trống"
  );

  isValidEmail =
    validatorNV.kiemTraRong("email", "tbEmail", "Email không được rỗng") &&
    validatorNV.kiemTraEmailHopLe("email", "tbEmail");

  isValidMatKhau =
    validatorNV.kiemTraRong(
      "password",
      "tbMatKhau",
      "Mật khẩu không được rỗng"
    ) && validatorNV.kiemTraMatKhau("password", "tbMatKhau");

  isValidNgayLam = validatorNV.kiemTraRong(
    "datepicker",
    "tbNgay",
    "Vui lòng điền ngày làm"
  );

  isValidLuong =
    validatorNV.kiemTraRong(
      "luongCB",
      "tbLuongCB",
      "Vui lòng nhập"
    ) && validatorNV.kiemTraLuong("luongCB", "tbLuongCB");

  isValidChucVu = validatorNV.kiemTraChucVu(
    "chucvu",
    "tbChucVu",
    "Chọn chức vụ"
  );

  isValidGioLam =
    validatorNV.kiemTraRong("gioLam", "tbGiolam", "Vui lòng không để trống") &&
    validatorNV.kiemTraGioLam("gioLam", "tbGiolam");

  return (
    isValidTK &&
    isValidHoTen &&
    isValidEmail &&
    isValidMatKhau &&
    isValidNgayLam &&
    isValidLuong &&
    isValidChucVu &&
    isValidGioLam
  );
}
